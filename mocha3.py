
# inに入力された文字列を格納
inp = input('確かめたい文字列は何ですか: ')
print("入力された文字列")
print(inp)
print("-------------------------------------------------------")


# 入力された文字列をリスト化
in_list = list(inp)


# 空の整合性確認用リストを定義
str_list = []
# 括弧のカウント
br_cnt = 0

# (){}[]の文字列を整合性確認用リストに格納
def store_br(symb):
    str_list.append(symb)

# 括弧の対応を確認し，True or Falseを返す
def is_c(open, close):
    openp = "({["
    closep = ")}]"
    return openp.find(open) == closep.find(close)

# 文字列の長さ分だけループ
for i in range(len(inp)):
    # 入力のi番目の文字列をsymbolに格納
    symbol = in_list[i]

    # 対応する文字列が--(){}[]--だった場合
    if symbol in "({[":
        store_br(symbol)
        br_cnt = br_cnt + 1

    # 対応する文字列が--(){}[]--以外だった場合
    if symbol in ")}]":
        store_br(symbol)
        br_cnt = br_cnt + 1

        # 文字列の内側から括弧の整合性を確認し，整合していれば整合性確認用リストから消去
        if is_c(str_list[br_cnt - 2], str_list[br_cnt - 1]) and br_cnt >= 2:
            str_list.pop(br_cnt - 1)
            str_list.pop(br_cnt - 2)
            # 整合性確認用リストから2つ括弧を消去した分カウントを2下げる
            br_cnt = br_cnt - 2

# 整合性確認用リストの中身が空か否かで文字列全体において括弧の整合性を判断
if not str_list:
    print("整合性〇")

else:
    print("整合性×")






