class stack:
    def __init__(self):
        self.items = []

    def is_empty(self):
        return self.items == []
    
    def push(self, item):
        self.items.append(item)

    def pop(self):
        return self.items.pop()

    def peek(self):
        return self.items[len(self.items) - 1]
    
    def size(self):
        return len(self.items)
    
def pb(gs):
    st = stack()
    balanced = True
    index = 0
    spc = 0
    while index < len(gs) and balanced:
        symbol = gs[index]
        if symbol == "(":
            st.push(symbol)
            spc = spc + 1
        elif symbol == ")":
            spc = spc + 1
            if not st.is_empty():
                st.pop()
            else:
                balanced = False
        
        else:
            pass
        index = index + 1

    if spc == 0:
        return False
    elif balanced and st.is_empty():
        return True
    else:
        return False


val = input('確かめたい文字列は何ですか: ')
print('----------------------------------------')

if pb(val) == True:
    print('()の整合性は〇')

else:
    print('()の整合性は×')


